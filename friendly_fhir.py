import warnings
from pandas import json_normalize
import pandas as pd
import glob
import os
import re
import json

# Pandas always throws a copy warning. Ignore it!
warnings.simplefilter(action='ignore', category=Warning)

def flatten_json(input_json, output, join=True):
    """Transform the input JSON file containing FHIR resources into relational tables and write resulting CSVs to output directory.

    :param(str or dict) input_json: Path to input file(s) [path to directory or file] in JSON FHIR format (utf-8 encoded)  _OR_ JSON dict
    :param(str) output: Path to existing directory to which resulting CSVs are written
    :param(bool) join: If set to True, an initial join is made at the end to reduce the number of CSV tables
    :return:
    """

    if not isinstance(input_json, dict):
        freddy = list()
        if os.path.isdir(input_json):
            for file in os.listdir(input_json):
                with open(input_json+"/"+file, encoding="utf-8") as data_file:
                    freddy.append(json.load(data_file))
        elif os.path.isfile(input_json):
            with open(input_json, encoding="utf-8") as data_file:
                freddy.append(json.load(data_file))
        else:
            raise TypeError("I cannot handle this type of input. Make sure to only feed paths to JSON files,"
          " paths to directories with JSON files or python dicts in JSON format.")
    else:
        freddy = [input_json]
    df = json_normalize(freddy)
    col_level2 = []
    for column_name, column_data in df.iteritems():
        for obj in column_data.values:
            if isinstance(obj, list):
                col_level2.append(column_name)
                break
                ### copy id to df_no_list
    df_no_list = df.drop(columns=col_level2)
    with open('{}/initial.csv'.format(output), 'w+') as file:
        df_no_list.to_csv(file, index=False)
    for col_name in df.columns:
        if re.match('^.*\\.id$|^id$|^.*\\.resourceType$|^resourceType$', col_name):
            col_level2.append(col_name)
    # level 1 to n
    _flatten_json_rec(df[col_level2], output, parent_column="initial", level=1)
    if join:
        _join_low2high(output)
    return


def _find_col_json_lists(df):
    """ Helper to find columns that still contain JSON arrays"""

    list_col_lists = list()
    for column_name, column_data in df.iteritems():
        for obj in column_data.values:
            if isinstance(obj, list):
                try:
                    _ = pd.json_normalize(obj[0])
                    list_col_lists.append(column_name)
                    break
                except:
                    pass
    return list_col_lists


def _flatten_json_rec(df, output_dir, parent_column, level=1):
    """Handle recursive call of the flattening."""

    last_id_cols = list()
    for col_name in df.columns:
        if re.match('^.*\\.id$|^id$|^.*\\.resourceType$|^resourceType$', col_name):
            if not re.match('^\\*\\*.*\\*\\*', col_name):
                # this id column is directly inherited from parent
                col_name_ad = "**{}(l{})**{}".format(parent_column, level - 1, col_name)
                df.rename(columns={col_name: col_name_ad}, inplace=True)
                col_name = col_name_ad
            last_id_cols.append(col_name)
    if len(df.columns) == len(last_id_cols):
        # only contains ids/keys
        return
    elif len(df.columns) < 1 or level > 10:
        raise Exception('Something went wrong or there are more than 10 nesting-levels in the JSON input. '
                        'Either way, I stopped the recursion. The output might still be correct.')

    for column_name, column_data in df.iteritems():
        if re.match('^.*\\.id$|^.*\\*\\*id$|^.*\\.resourceType$|^.*\\*\\*resourceType$', column_name):
            continue
        list_flattened_cells = []
        list_indices = []
        for idx, obj in enumerate(column_data.values):
            last_ids = df.loc[[idx], last_id_cols]
            last_ids = last_ids.loc[:, ~last_ids.columns.duplicated()]
            if isinstance(obj, list):
                # data cell contains list
                list_indices.append(idx)
                for list_elem in obj:
                    try:
                        list_elem_df = json_normalize(list_elem)
                    except:
                        # It is possible that there is a list of non-JSON elements
                        list_indices.pop(-1)
                        break
                    value_list = last_ids.values.tolist()[0]
                    for i, col in enumerate(last_ids.columns):
                        list_elem_df[col] = value_list[i]
                    list_flattened_cells.append(list_elem_df)
        rest_column = df[last_id_cols + [column_name]].drop(list_indices)
        if rest_column.count()[-1] > 0:
            list_flattened_cells.append(rest_column)
        df_flattened_column = pd.concat(list_flattened_cells, sort=False, ignore_index=True)

        still_bad_cols = _find_col_json_lists(df_flattened_column)
        df_no_list = df_flattened_column.drop(columns=still_bad_cols)
        new_id_cols = list()
        for col_name in df_flattened_column.columns:
            if re.match('^.*\\.id$|^.*\\.resourceType$', col_name) and not re.match('^\\*\\*.*\\*\\*', col_name):
                new_id_cols.append(col_name)
        if len(new_id_cols) < 1:
            new_id_cols = last_id_cols
        # Add all new IDs cols for the next recursion level
        still_bad_cols += new_id_cols
        id_cols = list(set(new_id_cols) | set(last_id_cols))
        # The following lines try to merge rows of the final output (rows with same ids)
        df_no_list = df_no_list.applymap(lambda x: (*x,) if isinstance(x, list) else x)
        df_non_mergable = df_no_list.groupby(id_cols, as_index=False).filter(lambda x: (x.nunique() > 1).any())
        df_merged = df_no_list.groupby(id_cols, as_index=False).filter(lambda x: (x.nunique() <= 1).all()).groupby(
            id_cols, as_index=False).first()
        df_no_list = pd.concat([df_non_mergable, df_merged], sort=False, ignore_index=True)
        # delete the lines above to increase performance (around 1/3 of runtime)
        df_no_list = df_no_list[id_cols + [col for col in df_no_list.columns if col not in id_cols]]
        # drop row that only contain NaNs (except for ids)
        with open('{}/level{}({})_{}.csv'.format(output_dir, str(level), parent_column, column_name), 'w+') as file:
            df_no_list.to_csv(file, index=False)
        _flatten_json_rec(df_flattened_column[still_bad_cols], output_dir, parent_column=column_name, level=level + 1)


def _join_low2high(in_out_dir):
    """Join low level tables to higher level tables if the low level contains only unique IDs (no additional rows)"""
    # do not parallelize!!
    filelist = glob.glob(os.path.join(in_out_dir, "*.csv"))
    for filename in sorted(filelist, reverse=True):
        if filename.endswith("initial.csv"):
            continue
        child_df = pd.read_csv(filename)
        id_cols = list()
        for col_name in child_df:
            if re.match('^\\*\\*.*\\*\\*.*id$|\\*\\*.*\\*\\*.*resourceType$', col_name):
                id_cols.append(col_name)
        if len(id_cols) < 1:
            continue
        if child_df.set_index(id_cols).index.is_unique:
            # child has unique row indices -> we can merge to parent without adding rows
            base_filename = os.path.basename(filename)
            level = int(base_filename[5])
            non_ids = set(child_df.columns) - set(id_cols)
            upper_key = base_filename.split(")_")[1].split(".csv")[0]
            rename_dict = {c: str(upper_key) + "." + c for c in non_ids}
            child_df.rename(columns=rename_dict, inplace=True)
            # Problem: id columns in parent can have different names (missing prefix)
            parent_col = base_filename[base_filename.find("(") + 1:base_filename.rfind(")")]
            name_changes = dict()
            for id_name in id_cols:
                if id_name.startswith("**{}(l{})**".format(parent_col, level - 1)):
                    # parent table has same column but without ** prefix
                    new_name = id_name.split("**{}(l{})**".format(parent_col, level - 1))[1]
                    name_changes[id_name] = new_name
            child_df.rename(columns=name_changes, inplace=True)
            for key in name_changes:
                id_cols.remove(key)
                id_cols.append(name_changes[key])
            if level > 1:
                parent_file = [s for s in filelist if
                               os.path.basename(s).startswith("level{}".format(level - 1)) and os.path.basename(
                                   s).endswith("_{}.csv".format(parent_col))][0]
            elif level == 1:
                parent_file = [s for s in filelist if os.path.basename(s).endswith("initial.csv")][0]
            else:
                continue
            parent_df = pd.read_csv(parent_file)
            merged_df = pd.merge(parent_df.astype({id: str for id in id_cols}),
                                 child_df.astype({id: str for id in id_cols}),
                                 how="outer", sort=False, on=id_cols)
            os.remove(filename)
            merged_df.to_csv(parent_file, index=False)
