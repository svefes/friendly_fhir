# Friendly FHIR  #

The project Friendly FHIR aims at tranforming JSON Bundles received through FHIR search into relational format (csv).
This tools does not know about resource types etc. Thus, the columns in the resulting tables only represent keys that are 
present in the input JSON.

### How do I use it? ###

It is written in plain python3

Dependencies: Pandas

* `import friendly_fhir as ff` 
* call `ff.flatten_json(input_json, output, join=True)` 
    * input_json: Path to JSON file(s) [file or directory] containing e.g. Bundle resources **OR** JSON python dict 
    * output: existing directory to which all generated tables (CSVs) are written
    * join: If set to `True`, it is tried to join tables from low levels to tables of higher ones to reduce the number of CSVs

_Output:_

Several CSVs written to `output` containing tables. The structure of the output is as follows.
     
     
     output
     +-- initial.csv
     +-- level1(initial)_exampleColumn1.csv
     ⋮
     +-- level1(initial)_exampleColumnX.csv
     +-- level2(exampleColumn1)_exampleColumn1.1.csv
     ⋮`
     
     
  where `initial.csv` contains the highest level of the flattened bundle. In the example the key/column `exampleColumn1` 
  contained at least one JSON array and could not be flattened in the initial step. This column is extracted and flattened 
  in a second step which results in output `level1(initial)_exampleColumn1.csv`.
  This recursion is repeated until no columns with JSON arrays are present any more (or more than 10 levels of nested arrays are found)

### Examples: ###

Adapt the paths before trying!    
    
```python
import friendly_fhir as ff

ff.flatten_json(input_json="/home/hpotter/darkMagic/fhir_pages",
                output="/home/hpotter/results", 
                join=True)
```
**OR** extract the bundle on the fly with the help of fhirclient (external tool) :

```python
import friendly_fhir as ff
from fhirclient import client
import fhirclient.models.fhirsearch as f
import fhirclient.models.procedure as r

settings = {'app_id': 'avada kedavra', 'api_base': 'https://vonk.fire.ly'}
smart = client.FHIRClient(settings=settings)
smart.prepare()

search = f.FHIRSearch(resource_type=r.Procedure(), struct={'status': 'completed'})
bundle = search.perform(smart.server)
json_obj = bundle.as_json()

ff.flatten_json(input_json=json_obj, output="/home/hpotter/results")
```